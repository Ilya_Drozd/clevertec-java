Get Method
=====================
###URL to get all users is
`
http://localhost:8887/user-operations/users
`

###URL to get user by id is
`
http://localhost:8887/user-operations/users/get-by?id=[userIdHere]
`

POST Method
=====================
###URL to add user
`http://localhost:8887/user-operations/add`

###Request's body example:
`
{
	"id":"1",
	"nickname":"nickname1",
	"country":"country1",
	"city":"city1",
	"login":"login1",
	"password":"password1"
}
`

PUT Method
=====================
###URL to update user
`
http://localhost:8887/user-operations/update
`
###Request's body example:
`
{
	"id":"1",
	"nickname":"nickname1",
	"country":"country1",
	"city":"city1",
	"login":"login1",
	"password":"password1"
}
`

DELETE Method
=====================
###URL to delete user
`
http://localhost:8887/user-operations/delete?id=[userIdHere]
`