package dao;

import org.junit.Test;
import annotations.proxy.MyProxy;
import user.User;

import java.lang.reflect.Proxy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CollectionDaoTest {

    private final CollectionDao testCollectionDao = new CollectionDao();
    private Dao testCollectionDaoProxy = (Dao) Proxy.newProxyInstance(CollectionDao.class.getClassLoader(),
            CollectionDao.class.getInterfaces(), new MyProxy(testCollectionDao, testCollectionDao.getClass().getName()));

    private final User testUser1 = new User(1, "nickname1", "country1", "city1", "login1", "password1");
    private final User testUser2 = new User(2, "nickname2", "country2", "city2", "login2", "password2");
    private final User testUser3 = new User(3, "nickname3", "country3", "city3", "login3", "password3");
    private final User testUser4 = new User(4, "nickname4", "country4", "city4", "login4", "password4");
    private final User testUser5 = new User(5, "nickname5", "country5", "city5", "login5", "password5");

    @Test
    public void addAndGetById() throws Exception {
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser2);
        testCollectionDaoProxy.add(testUser3);
        testCollectionDaoProxy.add(testUser4);
        testCollectionDaoProxy.add(testUser5);
        User gettingUser3 = testCollectionDaoProxy.getById(3);
        assertEquals(testUser3, gettingUser3);
    }

    @Test
    public void addTheSameUserSecondTime() throws Exception {
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser2);
        assertFalse(testCollectionDaoProxy.add(testUser1));
    }

    @Test
    public void addingUsersWithTheSameId() throws Exception {
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser2);
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser3);
        testCollectionDaoProxy.add(testUser4);
        testCollectionDaoProxy.add(testUser5);
        User gettingUser3 = testCollectionDaoProxy.getById(3);
        assertEquals(testUser3, gettingUser3);
    }

    @Test
    public void getUserById() throws Exception {
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser4);
        testCollectionDaoProxy.add(testUser2);
        testCollectionDaoProxy.add(testUser3);
        User gettingUser3 = testCollectionDaoProxy.getById(3);
        assertEquals(testUser3, gettingUser3);
    }

    @Test
    public void updateAndGetById() throws Exception {
        User testUpdatedUser3 = new User(3, "nickname33", "country33", "city33", "login33", "password33");
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser2);
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser3);
        testCollectionDaoProxy.add(testUser4);
        testCollectionDaoProxy.add(testUser5);
        testCollectionDaoProxy.update(testUpdatedUser3);
        User gettingUser3 = testCollectionDaoProxy.getById(3);
        assertEquals(gettingUser3, testUpdatedUser3);
    }

    @Test
    public void updateNotExistentUser() throws Exception {
        User testUpdatedUser3 = new User(3, "nickname33", "country33", "city33", "login33", "password33");
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser2);
        assertFalse(testCollectionDaoProxy.update(testUpdatedUser3));
    }

    @Test
    public void deleteUserAndUpdateIt() throws Exception {
        User testUpdatedUser3 = new User(3, "nickname33", "country33", "city33", "login33", "password33");
        testCollectionDaoProxy.add(testUser1);
        testCollectionDaoProxy.add(testUser2);
        testCollectionDaoProxy.add(testUser3);
        testCollectionDaoProxy.add(testUser4);
        testCollectionDaoProxy.add(testUser5);
        testCollectionDaoProxy.delete(3);
        assertFalse(testCollectionDaoProxy.update(testUpdatedUser3));
    }
}