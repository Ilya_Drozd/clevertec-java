package dao;

import annotations.proxy.MyProxy;
import org.junit.Test;
import org.mockito.Mockito;
import user.User;

import java.lang.reflect.Proxy;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CacheDaoTest {

    private final CacheDao testCacheDao = new CacheDao();
    private Dao testCacheDaoProxy = (Dao) Proxy.newProxyInstance(CacheDao.class.getClassLoader(),
            CacheDao.class.getInterfaces(), new MyProxy(testCacheDao, testCacheDao.getClass().getName()));
    private final DatabaseDAO databaseDaoMock = Mockito.mock(DatabaseDAO.class);

    @Test
    public void getByIdFromCache() throws Exception {
        User testUser1 = new User(1, "Nickname02", "Country1", "City1", "Login1", "Password1");

        testCacheDao.setDatabaseDao(databaseDaoMock);
        when(databaseDaoMock.getByIdFromDb(1)).thenReturn(testUser1);

        User gettingUser = testCacheDaoProxy.getById(1);
        assertEquals(gettingUser, testUser1);
    }

    @Test
    public void updateInCache() throws Exception {
        User testUser = new User(1, "Nickname1", "Country1", "City1", "Login1", "Password1");
        User testUserUpdated = new User(1, "Nickname01", "Country01", "City01", "Login01", "Password01");

        testCacheDao.setDatabaseDao(databaseDaoMock);
        when(databaseDaoMock.updateInDb(testUser)).thenReturn(true);
        when(databaseDaoMock.updateInDb(testUserUpdated)).thenReturn(true);

        testCacheDaoProxy.update(testUser);
        testCacheDaoProxy.update(testUserUpdated);
        User gettingUser = testCacheDaoProxy.getById(1);
        assertEquals(gettingUser, testUserUpdated);
    }

    @Test
    public void deleteFromCache() throws Exception {
        User testUser1 = new User(1, "Nickname1", "Country1", "City1", "Login1", "Password1");
        User testUser2 = new User(2, "Nickname2", "Country2", "City2", "Login2", "Password2");
        User testUser3 = new User(3, "Nickname3", "Country3", "City3", "Login3", "Password3");
        User testUser4 = new User(4, "Nickname4", "Country4", "City4", "Login4", "Password4");
        User newUser3 = new User(3, "Nickname00031", "Country000031", "City000031", "Login000031", "Password000031");

        testCacheDao.setDatabaseDao(databaseDaoMock);
        when(databaseDaoMock.updateInDb(testUser1)).thenReturn(true);
        when(databaseDaoMock.updateInDb(testUser2)).thenReturn(true);
        when(databaseDaoMock.updateInDb(testUser3)).thenReturn(true);
        when(databaseDaoMock.deleteInDb(3)).thenReturn(true);

        when(databaseDaoMock.updateInDb(testUser4)).thenReturn(true);

        testCacheDaoProxy.update(testUser1);
        testCacheDaoProxy.update(testUser2);
        testCacheDaoProxy.update(testUser3);
        testCacheDaoProxy.update(testUser4);
        testCacheDaoProxy.delete(3);

        //we added a new user with id:3 to the database without adding to the cache

        when(databaseDaoMock.getByIdFromDb(3)).thenReturn(newUser3);

        User gettingUser = testCacheDaoProxy.getById(3);
        assertEquals(gettingUser, newUser3);
    }
}

