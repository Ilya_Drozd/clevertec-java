package cache.logic;

import cache.Operations.UserCacheOperations;
import collection.MyArrayList;
import collection.MyList;
import org.junit.Test;
import user.CachingUser;
import user.User;

import static org.junit.Assert.*;

public class CacheLogicTest {

    private final CacheLogic testLogic = new CacheLogic();
    private final MyList<CachingUser> testCache = new MyArrayList<>(UserCacheOperations.getCacheCapacity());

    private final User testUser1 = new User(1, "Nickname1", "Country1", "City1", "Login1", "Password1");
    private final User testUser2 = new User(2, "Nickname2", "Country2", "City2", "Login2", "Password2");
    private final User testUser3 = new User(3, "Nickname3", "Country3", "City3", "Login3", "Password3");
    private final User testUser4 = new User(4, "Nickname4", "Country4", "City4", "Login4", "Password4");

    @Test
    public void add() {
        testLogic.add(testUser1, testCache);
        testLogic.add(testUser2, testCache);
        testLogic.add(testUser3, testCache);
        testLogic.add(testUser4, testCache);

        assertEquals(testCache.size(), 4);
    }

    @Test
    public void addInMiddle() {
        User testUser5 = new User(5, "Nickname5", "Country5", "City5", "Login5", "Password5");
        testLogic.add(testUser1, testCache);
        testLogic.add(testUser2, testCache);
        testLogic.add(testUser3, testCache);
        testLogic.add(testUser4, testCache);
        testLogic.add(2, testUser5, testCache);

        assertEquals(testUser5, testCache.get(2).getUser());
    }

    @Test
    public void deleteNotActualElement() {
        User testUser150 = new User(150, "Nickname150", "Country150", "City150", "Login150", "Password150");
        User testUser151 = new User(151, "Nickname151", "Country151", "City151", "Login151", "Password151");
        testLogic.add(testUser1, testCache);
        testLogic.add(testUser2, testCache);
        for(int i = 0; i < UserCacheOperations.getCacheCapacity() - 3; i++){
            testLogic.add(new User(i+3, "r", "r", "r", "r", "r"), testCache);
        }

        testLogic.add(testUser150, testCache);
        testLogic.add(testUser151, testCache);

        assertEquals(testUser151, testCache.get(0).getUser());
        assertEquals(testUser2, testCache.get(1).getUser());
        assertEquals(testUser150, testCache.get(99).getUser());
    }

    @Test
    public void deleteNotActualElementWithAddingInMiddleOfList() {
        User testUser150 = new User(150, "Nickname150", "Country150", "City150", "Login150", "Password150");
        User testUser151 = new User(151, "Nickname151", "Country151", "City151", "Login151", "Password151");
        User testUser152 = new User(152, "Nickname152", "Country152", "City152", "Login152", "Password152");
        testLogic.add(testUser1, testCache);
        testLogic.add(testUser2, testCache);
        for(int i = 0; i < UserCacheOperations.getCacheCapacity() - 4; i++){
            testLogic.add(new User(i+3, "r", "r", "r", "r", "r"), testCache);
        }

        testLogic.add(testUser150, testCache);
        testLogic.add(testUser151, testCache);
        testLogic.add(15, testUser152, testCache);

        assertEquals(testUser151, testCache.get(99).getUser());
        assertEquals(testUser2, testCache.get(0).getUser());
        assertEquals(testUser150, testCache.get(98).getUser());
        assertEquals(testUser152, testCache.get(15).getUser());
    }
}