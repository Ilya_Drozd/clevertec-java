package cache.Operations;

import annotations.proxy.MyProxy;
import org.junit.Test;
import user.User;

import java.lang.reflect.Proxy;

import static org.junit.Assert.*;

public class UserCacheOperationsTest {

    private final UserCacheOperations testOperations = new UserCacheOperations();
    private CacheOperations testOperationsProxy = (CacheOperations) Proxy.newProxyInstance(UserCacheOperations.class.getClassLoader(),
            UserCacheOperations.class.getInterfaces(), new MyProxy(testOperations, testOperations.getClass().getName()));

    private final User testUser1 = new User(1, "Nickname1", "Country1", "City1", "Login1", "Password1");
    private final User testUser2 = new User(2, "Nickname2", "Country2", "City2", "Login2", "Password2");
    private final User testUser3 = new User(3, "Nickname3", "Country3", "City3", "Login3", "Password3");
    private final User testUser4 = new User(4, "Nickname4", "Country4", "City4", "Login4", "Password4");

    @Test
    public void addInCacheAndTestInCacheById() {
        testOperationsProxy.addInCache(testUser1);
        testOperationsProxy.addInCache(testUser2);
        testOperationsProxy.addInCache(testUser3);
        testOperationsProxy.addInCache(testUser4);

        assertTrue(testOperationsProxy.inCache(1));
        assertTrue(testOperationsProxy.inCache(2));
        assertTrue(testOperationsProxy.inCache(3));
        assertTrue(testOperationsProxy.inCache(4));
        assertFalse(testOperationsProxy.inCache(14));
    }

    @Test
    public void addInCacheAndTestInCacheByUser() {
        User testUser40 = new User(40, "Nickname40", "Country40", "City40", "Login40", "Password40");
        testOperationsProxy.addInCache(testUser1);
        testOperationsProxy.addInCache(testUser2);
        testOperationsProxy.addInCache(testUser3);
        testOperationsProxy.addInCache(testUser4);

        assertTrue(testOperationsProxy.inCache(testUser1));
        assertTrue(testOperationsProxy.inCache(testUser2));
        assertTrue(testOperationsProxy.inCache(testUser3));
        assertTrue(testOperationsProxy.inCache(testUser4));
        assertFalse(testOperationsProxy.inCache(testUser40));
    }

    @Test
    public void updateInCache() {
        User testUser3Updated = new User(3, "Nickname03", "Country03", "City03", "Login03", "Password03");

        testOperationsProxy.addInCache(testUser1);
        testOperationsProxy.addInCache(testUser2);
        testOperationsProxy.addInCache(testUser3);
        testOperationsProxy.addInCache(testUser4);

        testOperationsProxy.updateInCache(testUser3Updated);
        assertEquals(testOperationsProxy.getByIdFromCache(3), testUser3Updated);
    }

    @Test
    public void getByIdFromCache() {
        testOperationsProxy.addInCache(testUser1);
        testOperationsProxy.addInCache(testUser2);
        testOperationsProxy.addInCache(testUser3);
        testOperationsProxy.addInCache(testUser4);

        assertEquals(testOperationsProxy.getByIdFromCache(1), testUser1);
        assertEquals(testOperationsProxy.getByIdFromCache(2), testUser2);
        assertEquals(testOperationsProxy.getByIdFromCache(3), testUser3);
        assertEquals(testOperationsProxy.getByIdFromCache(4), testUser4);
    }

    @Test
    public void deleteInCache() {
        testOperationsProxy.addInCache(testUser1);
        testOperationsProxy.addInCache(testUser2);
        testOperationsProxy.addInCache(testUser3);
        testOperationsProxy.addInCache(testUser4);

        testOperationsProxy.deleteInCache(2);
        assertEquals(testOperations.getCache().size(), 3);
    }
}