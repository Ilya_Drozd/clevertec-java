package collection;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyArrayListTest {

    @Test
    public void testAdd(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(5);
        myList.add(7);
        myList.add(6);
        int actual = myList.size();
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    public void testAddByIndex(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(7);
        myList.add(45);
        myList.add(9);
        myList.add(5);
        myList.add(1, 10);
        int actual = myList.get(1);
        int expected = 10;
        assertEquals(expected, actual);
    }

    @Test
    public void testGet(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(5);
        int actual = myList.get(0);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void testSize(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        int actual = myList.size();
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    public void testIndexOf(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        int actual = myList.indexOf(2);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void testRemoveByIndex(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.remove(1);
        int actual = myList.size();
        int expected = 2;
        assertEquals(expected, actual);
    }

    @Test
    public void testRemoveByElement(){
        MyArrayList<String> myList = new MyArrayList<>();
        myList.add("first");
        myList.add("second");
        myList.add("third");
        myList.remove("second");
        int actual = myList.size();
        int expected = 2;
        assertEquals(expected, actual);
    }

    @Test
    public void testClear(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(15);
        myList.add(25);
        myList.add(7);
        myList.clear();
        int actual = myList.size();
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void testContains(){
        MyArrayList<Integer> myList = new MyArrayList<>();
        myList.add(23);
        myList.add(32);
        myList.add(0);
        assertTrue(myList.contains(32));
    }
}