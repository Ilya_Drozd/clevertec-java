package annotations.proxy;

import annotations.AnnotationProcessor;
import annotations.Name;
import annotations.Time;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class MyProxy implements InvocationHandler {

    private static Logger log = Logger.getLogger(MyProxy.class.getName());
    private Object obj;
    private String name;

    public MyProxy(Object obj, String className) {
        this.obj = obj;
        this.name = className;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if(isMethodAnnotated(method, Name.class) && isMethodAnnotated(method, Time.class)){
            log.info("Method name - " + method.getName());
            return getTime(method, args);
        } else if (isMethodAnnotated(method, Name.class)) {
            log.info("Method name - " + method.getName());
            return method.invoke(obj, args);
        } else if (isMethodAnnotated(method, Time.class)) {
            return getTime(method, args);
        } else {
            return method.invoke(obj, args);
        }
    }

    private Object getTime(Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
        Date start = new Date();
        long startTime = start.getTime();
        Object o = method.invoke(obj, args);
        Date end = new Date();
        long endTime = end.getTime();
        log.info("Method execution time - " + (endTime - startTime) + "ms");
        return o;
    }

    private boolean isMethodAnnotated(Method method, Class<? extends Annotation> annotation) {
        AnnotationProcessor processor = new AnnotationProcessor();
        List<String> list = processor.getAnnMethod(name, annotation);
        for (String methodName : list) {
            if (methodName.equals(method.getName())) {
                return true;
            }
        }
        return false;
    }
}
