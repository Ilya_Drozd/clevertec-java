package annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnnotationProcessor {

    private static Logger log = Logger.getLogger(AnnotationProcessor.class.getName());

    public List<String> getAnnMethod(String className, Class<? extends Annotation> annotation){
        List<String> annMethods = new ArrayList<>();
        Class clazz;
        try {
            clazz = Class.forName(className);
            Method[] methods = clazz.getDeclaredMethods();
            for(Method method: methods){
                if (method.isAnnotationPresent(annotation)){
                    annMethods.add(method.getName());
                }
            }
        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, "Class not found", e.getMessage());
        }
        return annMethods;
    }
}
