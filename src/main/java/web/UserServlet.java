package web;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.CacheDao;
import dao.CollectionDao;
import dao.Dao;
import user.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "userServlet", urlPatterns = "/user-operations/*")
public class UserServlet extends HttpServlet {

    private static final User BAD_USER = new User(null, "incorrect", "incorrect", "incorrect", "incorrect",
            "incorrect");
    private static Logger log = Logger.getLogger(UserServlet.class.getName());

    private Dao dao = null;
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void init() throws ServletException {
        try {
            FileInputStream fis = new FileInputStream("src/main/resources/project.properties");
            Properties property = new Properties();
            property.load(fis);

            String daoType = property.getProperty("daoType");

            if (daoType.equalsIgnoreCase("H2")){
                dao = new CacheDao();
            }
            else{
                dao = new CollectionDao();
            }
            super.init();
        } catch (IOException e) {
            log.log(Level.SEVERE, "File not found", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String path = req.getPathInfo();
        switch (path) {
            case "/users":
                try {
                    List<User> list = dao.getAll();
                    resp.getWriter().print(mapper.writeValueAsString(list));
                    log.info("Getting users success");
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Getting users error", e);
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
                break;
            case "/users/get-by":
                int id = Integer.parseInt(req.getParameter("id"));
                resp.getWriter().print("User");
                try {
                    User user = dao.getById(id);
                    resp.getWriter().print(mapper.writeValueAsString(user));
                    log.info("Getting user success");
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Getting user error", e);
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
                break;
            default:
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String path = req.getPathInfo();
        if (path.equals("/add")) {
            User user = getUserFromRequest(req, resp);
            if (!user.equals(BAD_USER)) {
                try {
                    if (dao.add(user)) {
                        log.info("Adding user success");
                    }
                    else {
                        log.info("Adding user error");
                        resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Adding user error", e);
                    resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                }
            } else {
                resp.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            }
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String path = req.getPathInfo();
        if (path.equals("/update")) {
            User user = getUserFromRequest(req, resp);
            if (!user.equals(BAD_USER)) {
                try {
                    if (dao.update(user)) {
                        log.info("Updating user success");
                    } else {
                        log.info("Updating user do not found");
                        resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Updating user error", e);
                    resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
                }
            } else {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String path = req.getPathInfo();
        if (path.equals("/delete")) {
            try {
                int id = Integer.parseInt(req.getParameter("id"));
                if (dao.delete(id)) {
                    log.info("Deleting user success");
                } else {
                    log.info("Deleting user don't found");
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "Deletion user error", e);
                resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
            }
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private User getUserFromRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String stringUser = String.valueOf(buffer);
        User user;
        try {
            user = mapper.readValue(stringUser, User.class);
        } catch (IOException e) {
            log.log(Level.SEVERE, "Data entered incorrectly", e);
            return BAD_USER;
        }
        return user;
    }
}
