package database;

public class SqlQueries {
    private static final String MAKING_TABLE = "CREATE TABLE \"USER\" \n" +
            "(\n" +
            "\t\"ID\" BIGINT NOT NULL,\n" +
            "\t\"NICKNAME\" VARCHAR(255) NOT NULL,\n" +
            "\t\"COUNTRY\" VARCHAR(255) NOT NULL,\n" +
            "\t\"CITY\" VARCHAR(255) NOT NULL,\n" +
            "\t\"LOGIN\" VARCHAR(255) NOT NULL,\n" +
            "\t\"PASSWORD\" VARCHAR(255) NOT NULL,\n" +
            "\tCONSTRAINT USER_PKEY PRIMARY KEY (\"ID\")\n" +
            ")";
    private static final String INSERT = "INSERT INTO USER (ID, NICKNAME, COUNTRY, CITY, LOGIN, PASSWORD) VALUES(?, ?, ?, ?, ?, ?)";
    private static final String SELECT_ALL = "SELECT ID, NICKNAME, COUNTRY, CITY, LOGIN, PASSWORD FROM USER";
    private static final String SELECT = "SELECT ID, NICKNAME, COUNTRY, CITY, LOGIN, PASSWORD FROM USER WHERE ID=?";
    private static final String UPDATE = "UPDATE USER SET NICKNAME=?, COUNTRY=?, CITY=?, LOGIN=?, PASSWORD=? WHERE ID=?";
    private static final String DELETE = "DELETE FROM USER WHERE ID=?";

    public static String getMakingTable() {
        return MAKING_TABLE;
    }

    public static String getInsert() {
        return INSERT;
    }

    public static String getSelectAll() {
        return SELECT_ALL;
    }

    public static String getSelect() {
        return SELECT;
    }

    public static String getUpdate() {
        return UPDATE;
    }

    public static String getDelete() {
        return DELETE;
    }
}
