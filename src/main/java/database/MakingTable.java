package database;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MakingTable {

    private static Logger log = Logger.getLogger(MakingTable.class.getName());
    private ConnectionProvider connectionProvider = new ConnectionProvider();

    public void createDb() throws Exception {
        String sql = SqlQueries.getMakingTable();
        try (Connection connection = connectionProvider.getConnection(); Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "MakingTable table error", e);
            throw new Exception(e);
        }
    }
}
