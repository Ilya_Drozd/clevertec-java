package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionProvider {
    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:./user";
    private static final String DB_USERNAME = "ss";
    private static final String DB_PASSWORD = "";

    private static Logger log = Logger.getLogger(ConnectionProvider.class.getName());

    public Connection getConnection() throws Exception {
        Connection connection = null;
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            log.info("Connection created");
        } catch (ClassNotFoundException | SQLException e) {
            log.log(Level.SEVERE,"Connection error");
            throw new Exception(e);
        }
        return connection;
    }
}
