package user;

import java.util.Objects;

public class CachingUser{

    private User user;
    private long check;

    public CachingUser(User user) {
        this.user = user;
        this.check = check;
    }

    public long getCheck() {
        return check;
    }

    public void setCheck(long check) {
        this.check = check;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CachingUser)) return false;
        if (!super.equals(o)) return false;
        CachingUser that = (CachingUser) o;
        return check == that.check;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), check);
    }

    @Override
    public String toString() {
        return "CachingUser{" +
                "check=" + check +
                '}';
    }
}
