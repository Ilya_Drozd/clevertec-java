package collection;

public interface MyList<E> {
    void add(E element);
    void add(int index, E element);
    E get(int index);
    int size();
    int indexOf(E element);
    void remove(int index);
    void remove(E element);
    void clear();
    boolean contains(E element);
}
