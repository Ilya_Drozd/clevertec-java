package collection;

public class MyArrayList<E> implements MyList<E> {

    private final static int START_CAPACITY = 10;
    private int size;
    private int capacity;
    private E[] array;


    public MyArrayList() {
        this.array = (E[])(new Object[START_CAPACITY]);
        this.capacity = START_CAPACITY;
    }

    public MyArrayList(int startCapacity) {
        this.array = (E[])(new Object[startCapacity]);
        this.capacity = startCapacity;
    }

    @Override
    public void add(E element) {
        if(ensureCapacity(size + 1)){
            array[size] = element;
            ++size;
        }
        else{
            makingBigArray(array)[size] = element;
            ++size;
        }
    }

    public boolean ensureCapacity(int size){
        return size <= array.length;
    }

    public E[] makingBigArray(E[] smallArray){
        capacity = (capacity * 3) / 2 + 1;
        E[] bigArray = (E[])(new Object[capacity]);
        System.arraycopy(array, 0, bigArray, 0, array.length);
        array = (E[])(new Object[capacity]);
        System.arraycopy(bigArray, 0, array, 0, bigArray.length);
        return array;
    }

    @Override
    public void add(int index, E element) {
        if(ensureCapacity(size + 1)){
            System.arraycopy(array, index, array, index + 1, size - index);
            array[index] = element;
            ++size;
        }
        else{
            array = makingBigArray(array);
            System.arraycopy(array, index, array, index + 1, size - index);
            array[index] = element;
            ++size;
        }
    }

    @Override
    public E get(int index) {
        return array[index];
    }

    @Override
    public int size(){
        return size;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        for(int i = 0; i < size; i++){
            if(element.equals(array[i])){
                index = i;
                break;
            }
            else{
                index = -1;
            }
        }
        return index;
    }

    @Override
    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, size - (index + 1));
        array[--size] = null;
    }

    @Override
    public void remove(E element) {
        for(int i = 0; i < size; i++){
            if(element.equals(array[i])){
                System.arraycopy(array, i + 1, array, i, size - (i + 1));
                array[--size] = null;
            }
        }
    }

    @Override
    public void clear() {
        array = (E[])(new Object[START_CAPACITY]);
        size = 0;
    }

    @Override
    public boolean contains(E element){
        boolean b = true;
        for(int i = 0; i < size; i++){
            if(element.equals(array[i])){
                b = true;
                break;
            }
            else{
                b = false;
            }
        }
        return b;
    }
}
