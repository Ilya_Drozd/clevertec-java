package dao;

import database.ConnectionProvider;
import database.SqlQueries;
import user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseDAO implements IDatabaseDao {

    private static Logger log = Logger.getLogger(DatabaseDAO.class.getName());
    private ConnectionProvider connectionProvider = new ConnectionProvider();
    private Connection connection = null;

    @Override
    public boolean addInDb(User user) throws Exception {
        int check;
        connection = connectionProvider.getConnection();
        String sql = SqlQueries.getInsert();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            connection.setAutoCommit(false);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(2, user.getNickname());
            preparedStatement.setString(3, user.getCountry());
            preparedStatement.setString(4, user.getCity());
            preparedStatement.setString(5, user.getLogin());
            preparedStatement.setString(6, user.getPassword());
            check = preparedStatement.executeUpdate();
            connection.commit();
            log.info("Add to database successfully");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Adding to database error", e);
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.setAutoCommit(true);
            if (connection != null) {
                connection.close();
            }
        }
        return check == 1;
    }

    @Override
    public List<User> getAll() throws Exception {
        connection = connectionProvider.getConnection();
        List<User> userList = new ArrayList<>();
        String sql = SqlQueries.getSelectAll();

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("ID"));
                user.setNickname(resultSet.getString("NICKNAME"));
                user.setCountry(resultSet.getString("COUNTRY"));
                user.setCity(resultSet.getString("CITY"));
                user.setLogin(resultSet.getString("LOGIN"));
                user.setPassword(resultSet.getString("PASSWORD"));
                userList.add(user);
            }
            log.info("Getting all users from database successfully");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Getting all user from database error", e);
            throw new Exception(e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return userList;
    }

    @Override
    public User getByIdFromDb(int id) throws Exception {
        connection = connectionProvider.getConnection();
        String sql = SqlQueries.getSelect();
        User user = new User();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("ID"));
                user.setNickname(resultSet.getString("NICKNAME"));
                user.setCountry(resultSet.getString("COUNTRY"));
                user.setCity(resultSet.getString("CITY"));
                user.setLogin(resultSet.getString("LOGIN"));
                user.setPassword(resultSet.getString("PASSWORD"));
            }
            if(user.getId() != null){
                log.info("Getting user from database successfully");
            }
            else {
                log.log(Level.SEVERE, "Getting non-existent user from database");
                throw new Exception();
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Getting user from database error", e);
            throw new Exception(e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return user;
    }

    @Override
    public boolean updateInDb(User user) throws Exception {
        int check;
        connection = connectionProvider.getConnection();
        String sql = SqlQueries.getUpdate();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql);){
            connection.setAutoCommit(false);
            preparedStatement.setString(1, user.getNickname());
            preparedStatement.setString(2, user.getCountry());
            preparedStatement.setString(3, user.getCity());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setInt(6, user.getId());
            check = preparedStatement.executeUpdate();
            connection.commit();
            if(check == 0)
                log.info("Updating non-existent object in database");
            else
                log.info("Update in database successfully");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Updating in database error", e);
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.setAutoCommit(true);
            if (connection != null) {
                connection.close();
            }
        }
        return check == 1;
    }

    @Override
    public boolean deleteInDb(int id) throws Exception {
        int check;
        connection = connectionProvider.getConnection();

        String sql = SqlQueries.getDelete();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            check = preparedStatement.executeUpdate();
            if (check == 0)
                log.info("Deleting non-existent object in database");
            else
                log.info("Delete from database successfully");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Deletion from database error", e);
            throw new Exception(e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return check == 1;
    }
}
