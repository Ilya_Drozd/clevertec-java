package dao;

import user.User;

import java.util.List;

public interface IDatabaseDao {
    boolean addInDb(User user) throws Exception;
    List<User> getAll() throws Exception;
    User getByIdFromDb(int key) throws Exception;
    boolean updateInDb(User user) throws Exception;
    boolean deleteInDb(int key) throws Exception;
}

