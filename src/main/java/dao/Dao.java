package dao;

import user.User;

import java.util.List;

public interface Dao {
    boolean add(User user) throws Exception;
    List<User> getAll() throws Exception;
    User getById(int key) throws Exception;
    boolean update(User user) throws Exception;
    boolean delete(int key) throws Exception;
}
