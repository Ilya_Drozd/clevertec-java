package dao;

import annotations.Name;
import annotations.Time;
import cache.Operations.UserCacheOperations;
import user.User;

import java.util.List;

public class CacheDao implements Dao {

    private UserCacheOperations operationsWithCache = new UserCacheOperations();
    private DatabaseDAO databaseDao = new DatabaseDAO();

    @Override
    @Name
    @Time
    public boolean add(User user) throws Exception {
        return databaseDao.addInDb(user);
    }

    @Override
    @Name
    @Time
    public List<User> getAll() throws Exception {
        return databaseDao.getAll();
    }

    @Override
    @Name
    @Time
    public User getById(int id) throws Exception {
        User user = operationsWithCache.getByIdFromCache(id);
        if(user == null){
            user = databaseDao.getByIdFromDb(id);
            operationsWithCache.addInCache(user);
        }
        return user;
    }

    @Override
    @Name
    @Time
    public boolean update(User user) throws Exception {
        if (databaseDao.updateInDb(user)){
            if (operationsWithCache.inCache(user)) {
                operationsWithCache.updateInCache(user);
            } else {
                operationsWithCache.addInCache(user);
            }
            return true;
        }
        return false;
    }

    @Override
    @Name
    @Time
    public boolean delete(int id) throws Exception {
        if(databaseDao.deleteInDb(id)){
            if(operationsWithCache.inCache(id)){
                operationsWithCache.deleteInCache(id);
            }
            return true;
        }
        return false;
    }

    void setDatabaseDao(DatabaseDAO databaseDao) {
        this.databaseDao = databaseDao;
    }
}
