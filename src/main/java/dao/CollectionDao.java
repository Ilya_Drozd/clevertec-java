package dao;

import annotations.Name;
import annotations.Time;
import collection.MyArrayList;
import collection.MyList;
import user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CollectionDao implements Dao {

    private MyList<User> collection = new MyArrayList<>();
    private static Logger log = Logger.getLogger(CollectionDao.class.getName());

    @Override
    @Name
    @Time
    public boolean add(User user) {
        boolean check = true;
        int id = user.getId();
        for (int i = 0; i < collection.size(); i++) {
            int index = collection.get(i).getId();
            if (id == index) {
                check = false;
                break;
            }
        }
        if (check) {
            collection.add(user);
            log.info("Adding user in collection");
            return true;
        } else {
            log.info("Could not add in collection: user already exists");
            return false;
        }
    }

    @Override
    @Name
    @Time
    public List<User> getAll() {
        List<User> list = new ArrayList<>();
        try{
            for (int i = 0; i < collection.size(); i++) {
                list.add(collection.get(i));
            }
            log.info("Getting all users from collection successfully");
            return list;
        } catch (Exception e){
            log.log(Level.SEVERE, "Getting all user from collection error", e);
            throw e;
        }
    }

    @Override
    @Name
    @Time
    public User getById(int key) throws Exception {
            User user = null;
            for(int i = 0; i < collection.size(); i++){
                int id = collection.get(i).getId();
                if (key == id){
                    user = collection.get(i);
                    break;
                }
            }

            if(user != null){
                log.info("Getting user from collection");
                return user;
            } else {
                log.info("Getting user from collection error");
                throw new Exception();
            }
    }

    @Override
    @Name
    @Time
    public boolean update(User user) {
        boolean check = false;
        int index = 0;
        int id = user.getId();
        for (int i = 0; i < collection.size(); i++) {
            index = collection.get(i).getId();
            if (id == index) {
                index = i;
                check = true;
                break;
            }
        }
        if (check) {
            collection.remove(index);
            collection.add(index, user);
            log.info("Updating user in collection");
            return true;
        } else {
            log.info("Could not update in collection: user does not exist");
            return false;
        }
    }

    @Override
    @Name
    @Time
    public boolean delete(int key) {
        try{
            boolean check = false;
            for(int i = 0; i < collection.size(); i++){
                int id = collection.get(i).getId();
                if (key == id){
                    collection.remove(i);
                    check = true;
                    break;
                }
            }
            if(check){
                log.info("User deleted from collection");
                return true;
            } else{
                log.info("User deleted from collection");
                return false;
            }
        } catch (Exception e){
            log.log(Level.SEVERE, "Deletion from collection error", e);
            return false;
        }
    }
}
