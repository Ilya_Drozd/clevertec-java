package cache.logic;

import cache.Operations.UserCacheOperations;
import collection.MyList;
import user.CachingUser;
import user.User;

public class CacheLogic implements ICacheLogic {

    private static final int CACHE_CAPACITY = UserCacheOperations.getCacheCapacity();
    private static long check = 0;

    @Override
    public void add(User user, MyList<CachingUser> cache) {
        CachingUser cachingUser = new CachingUser(user);
        cachingUser.setCheck(getActualCheck());
        if(cache.size() < CACHE_CAPACITY){
            cache.add(cachingUser);
        }
        else {
            deleteNotActualElement(cache);
            cache.add(0, cachingUser);
        }
    }

    @Override
    public void add(int key, User user, MyList<CachingUser> cache) {
        CachingUser cachingUser = new CachingUser(user);
        cachingUser.setCheck(getActualCheck());
        if(cache.size() < CACHE_CAPACITY){
            cache.add(key, cachingUser);
        }
        else {
            deleteNotActualElement(cache);
            cache.add(key, cachingUser);
        }
    }


    @Override
    public void deleteNotActualElement(MyList<CachingUser> cache) {
        int id = 0;
        long minTime = cache.get(0).getCheck();
        for(int i = 0; i < cache.size(); i++){
            if(cache.get(i).getCheck() <= minTime){
                minTime = cache.get(i).getCheck();
                id = cache.get(i).getUser().getId();
            }
        }
        for(int i = 0; i < cache.size(); i++){
            if(cache.get(i).getUser().getId() == id){
                cache.remove(i);
            }
        }
    }

    @Override
    public long getActualCheck() {
        return ++check;
    }
}
