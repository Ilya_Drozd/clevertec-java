package cache.logic;

import collection.MyList;
import user.CachingUser;
import user.User;

public interface ICacheLogic {
    void add(User user, MyList<CachingUser> list);
    void add(int key, User user, MyList<CachingUser> list);
    void deleteNotActualElement(MyList<CachingUser> list);
    long getActualCheck();
}
