package cache.Operations;

import collection.MyList;
import user.CachingUser;
import user.User;

public interface CacheOperations {
    boolean inCache(int id);
    boolean inCache(User user);
    void addInCache(User user);
    User getByIdFromCache(int id);
    void updateInCache(User user);
    void deleteInCache(int id);
}
