package cache.Operations;

import annotations.Name;
import annotations.Time;
import cache.logic.CacheLogic;
import collection.MyArrayList;
import collection.MyList;
import user.CachingUser;
import user.User;

import java.util.logging.Logger;

public class UserCacheOperations implements CacheOperations {

    private static final int CACHE_CAPACITY = 100;
    private MyList<CachingUser> cache = new MyArrayList<>(CACHE_CAPACITY);

    private CacheLogic cacheLogic = new CacheLogic();
    private static Logger log = Logger.getLogger(UserCacheOperations.class.getName());

    @Override
    @Name
    @Time
    public boolean inCache(int id) {
        int index;
        for(int i = 0; i < cache.size(); i++){
            index = cache.get(i).getUser().getId();
            if(id == index){
                return true;
            }
        }
        return false;
    }

    @Override
    @Name
    @Time
    public boolean inCache(User user) {
        int index;
        int id = user.getId();
        for(int i = 0; i < cache.size(); i++){
            index = cache.get(i).getUser().getId();
            if(id == index){
                return true;
            }
        }
        return false;
    }

    @Override
    @Name
    @Time
    public void addInCache(User user) {
        int id = user.getId();
        boolean check = true;
        int i = 0;
        while (i < cache.size()){
            int count = cache.get(i).getUser().getId();
            if(id == count){
               check = false;
               break;
            }
            i++;
        }
        if(check){
            cacheLogic.add(user, cache);
            log.info("Adding user in cache");
        }
        else{
            log.info("Could not add in cache: object already exists");
        }
    }

    @Override
    @Name
    @Time
    public void updateInCache(User user) {
        int id = user.getId();
        boolean check = false;
        int i = 0;
        int count;
        int index = 0;
        while (i < cache.size()){
            count = cache.get(i).getUser().getId();
            index = cache.indexOf(cache.get(i));
            if(id == count){
                check = true;
                break;
            }
            i++;
        }
        if(check){
            deleteInCache(user.getId());
            cacheLogic.add(index, user, cache);
            log.info("Adding user in cache");
        }
        else{
            log.info("Could not update in cache: object does not exist");
        }
    }

    @Override
    @Name
    @Time
    public User getByIdFromCache(int id) {
        int i = 0;
        while (i < cache.size()){
            int count = cache.get(i).getUser().getId();
            if(id == count){
                log.info("Getting user from cache");
                return cache.get(i).getUser();
            }
            i++;
        }
        return null;
    }

    @Override
    @Name
    @Time
    public void deleteInCache(int id) {
        int i = 0;
        while(i < cache.size()){
            int count = cache.get(i).getUser().getId();
            if(id == count){
                cache.remove(i);
                log.info("User deleted from cache");
            }
            i++;
        }
    }

    MyList<CachingUser> getCache() {
        return cache;
    }

    public static int getCacheCapacity() {
        return CACHE_CAPACITY;
    }
}
